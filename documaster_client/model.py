import datetime
from abc import ABC
from enum import Enum
from typing import Dict, Optional, List, Union, Any

import yaml
from pydantic import BaseModel, Field

from documaster_client.util import to_lower_camel

DATE_FORMAT = "yyyy-MM-dd"
TIMESTAMP_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSXX"

CODE_LIST_TYPES = {
    "administrativEnhet": {"Korrespondansepart", "Moeteregistrering", "Saksmappe"},
    "dokumentmedium": {},
}


class FUPItem(BaseModel):
    funksjon: str
    under_funksjon: str
    prosess: str


class NoarkCode(BaseModel):
    code: Optional[str]
    name: Optional[str]
    description: Optional[str]


class DocumasterConfigItem(BaseModel):
    id: Optional[str]  # id
    indent: Optional[str]
    navn: Optional[str]


class DocumasterConfigViewItem(BaseModel):
    id: Optional[str]  # id
    navn: str


class ArkivskaperConfig(BaseModel):
    id: Optional[str]  # id
    indent: str
    navn: str


class ArkivConfig(BaseModel):
    id: Optional[str]  # id
    indent: str
    navn: str


class ArkivdelConfig(BaseModel):
    id: Optional[str]  # id
    indent: str
    navn: str


class PrimKlasseConfig(BaseModel):
    id: Optional[str]  # id
    indent: str
    navn: str


class CodeListConfig(BaseModel):
    cache: bool = False
    field_names: Optional[List[str]]
    type: Optional[str]


class DocumasterClientConfig(BaseModel):
    url: str
    headers: Dict[str, str]
    arkiv_skaper: ArkivskaperConfig
    arkiv: Optional[DocumasterConfigItem]
    arkivdeler: Optional[DocumasterConfigItem]
    klasse: Optional[DocumasterConfigItem]
    retry_max = 5
    code_list: CodeListConfig


class DocumasterClientConfigLoader(BaseModel):
    documaster_client: DocumasterClientConfig

    @classmethod
    def from_yaml(cls, yamlstr: str) -> "DocumasterClientConfigLoader":
        return cls(**yaml.load(yamlstr, Loader=yaml.FullLoader))

    @classmethod
    def from_file(cls, filename: str) -> "DocumasterClientConfigLoader":
        with open(filename, "r") as f:
            return cls.from_yaml(f.read())


class DocumasterAction:
    action: str


class ActionLinkItem(ABC, BaseModel):
    action: str
    type: str
    id: str
    ref: str
    linkToId: str


class ActionItem(ABC, BaseModel):
    action: str
    type: str
    id: str
    fields: Dict[str, Union[str, datetime.datetime, datetime.date]]


class NoarkEntity(BaseModel):
    id: Optional[str]
    uuid: Optional[str]
    version: Optional[str]
    opprettet_av: Optional[str]
    opprettet_av_bruker_ident: Optional[str]
    opprettet_dato: Optional[datetime.datetime]

    links: Optional[Dict[str, Union[str, List[str]]]] = {}
    _tmp_id: Optional[str]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True
        orm_mode = True


class AbstractRegistering(BaseModel):
    registrerings_ident: Optional[str]
    tittel: Optional[str]
    offentlig_tittel: Optional[str]
    beskrivelse: Optional[str]
    forfatter: Optional[str]

    avsluttet_dato: Optional[datetime.datetime]  # the resp is in timestamp format
    avsluttet_av: Optional[str]
    avsluttet_av_bruker_ident: Optional[str]

    mappe_link: str = "refMappe"
    ekstern_id_link: str = "refEksternId"
    arkivdel_link: str = "refArkivdel"
    primaer_klasse_link: str = "refPrimaerKlasse"
    sekundaer_klasse_link: str = "refSekundaerKlasse"
    korrespondansepart_link: str = "refKorrespondansepart"
    dokument_link: str = "refDokument"
    noekkelord_link: str = "refNoekkelord"
    merknad_link: str = "refMerknad"
    nasjonal_identifikator_link: str = "refNasjonalIdentifikator"
    kryssreferanse_til_mappe_link: str = "refKryssreferanseTilMappe"
    kryssreferanse_fra_mappe_link: str = "refKryssreferanseFraMappe"
    kryssreferanse_til_registrering_link: str = "refKryssreferanseTilRegistrering"
    kryssreferanse_fra_registrering_link: str = "refKryssreferanseFraRegistrering"


class SaveAction(ActionItem):
    action: str = "save"


class DeleteAction(ActionItem):
    action: str = "delete"


class LinkAction(ActionLinkItem):
    action: str = "link"


class ToDocumaster(BaseModel):
    class Config:
        allow_population_by_field_name = True


class TransactionRequest(BaseModel):
    actions: List[Union[SaveAction, LinkAction]]


class SortOrder(ToDocumaster):
    field: str
    order: str = "desc"


class Oper(str, Enum):
    AND = "&&"
    OR = "||"


class CompOper(str, Enum):
    NOT_LIKE = "!="
    LIKE = "="
    WLIKE = "%="


class ValueOperator(BaseModel):
    field_value: Union[None, str, int, float, bool]
    oper: Optional[Oper]
    comp_oper: Optional[CompOper] = CompOper.LIKE  # default ==


class QueryRequest(ToDocumaster):
    type: str
    limit: int
    query: str
    joins: Optional[Dict[str, str]]
    parameters: Optional[Dict[str, Any]]
    sort_order: Optional[SortOrder] = Field(alias="sortOrder")

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class CodeListResp(BaseModel):
    type: str
    field: str
    values: List[NoarkCode]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class CodeListRespHolder(BaseModel):
    results: List[CodeListResp]


class RespItem(BaseModel):
    type: str
    id: str
    version: str
    fields: Union[Dict[str, Union[str, Dict[str, Any]]]]
    links: Dict[str, int]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class TransactionResp(BaseModel):
    saved: Dict[str, RespItem]
    deleted: Any


class QueryResp(BaseModel):
    has_more: bool
    results: List[RespItem]

    class Config:
        alias_generator = to_lower_camel
        allow_population_by_field_name = True


class Arkivdelstatus(str, Enum):
    aktiv_periode = "A"  # "Aktiv periode"
    overlappingsperiode = "O"  # "Overlappingsperiode"
    avsluttet_periode = "P"  # "Avsluttet periode"
    uaktuelle_mapper = "U"  # "Uaktuelle mapper"


custom_encoder = lambda obj: dict(_type=type(obj).__name__, **obj.dict())


class AdministrativEnhet(NoarkCode):
    class Config:
        json_encoders = {"AdministrativEnhet": custom_encoder}
        json_decoders = {"AdministrativEnhet": custom_encoder}


class Saksstatus(str, Enum):
    under_behandling = "B"
    avsluttet = "A"
    utgaar = "U"
    opprettet_av_saksbehandler = "R"
    unntatt_prosesstyring = "P"
    avsluttet_av_saksbehandler = "S"
    ferdig_fra_saksbehandler = "F"


class SkjermingType(NoarkCode):
    vals: Dict[str, str]  # should come from config


class Skjerming(NoarkCode):
    pass


class Arkiv(NoarkEntity):
    tittel: Optional[str]
    beskrivelse: Optional[str]

    arkivskaper_link = "refArkivskaper"


class Arkivdel(NoarkEntity):
    tittel: str
    beskrivelse: Optional[str]
    arkivperiode_start_dato: Optional[Union[datetime.datetime, datetime.date]] = Field(
        alias="arkivperiodeSluttDato"
    )
    # the resp is in timestamp format
    arkivperiode_slutt_dato: Optional[Union[datetime.datetime, datetime.date]] = Field(
        alias="arkivperiodeSluttDato"
    )
    # the resp is in timestamp format
    arkivdelstatus: Optional[Arkivdelstatus]
    skjerming: Optional[Skjerming]

    arkiv_link = "refArkiv"
    mappe_link = "refMappe"
    registrering_link = "refRegistrering"
    primaer_klassifikasjonssystem_link = "refPrimaerKlassifikasjonssystem"
    sekundaer_klassifikasjonssystem_link = "refSekundaerKlassifikasjonssystem"


class Arkivskaper(NoarkEntity):
    arkivskaper_ident: Optional[str] = Field(alias="arkivskaperIdent")
    arkivskaper_navn: Optional[str] = Field(alias="arkivskaperNavn")


class Klasse(NoarkEntity):
    klasse_ident: str
    tittel: str
    avsluttetDato: Optional[datetime.datetime]

    klassifikasjonssystem_link = "refKlassifikasjonssystem"
    forelder_klasse_link = "refForelderKlasse"
    mappe_link_som_primaer = "refMappeSomPrimaer"
    mappe_link_som_sekundaer = "refMappeSomSekundaer"
    registrering_som_primaer_link = "refRegistreringSomPrimaer"
    registrering_som_sekundaer_link = "refRegistreringSomSekundaer"

    def __str__(self):
        return "Id: %s, ident: %s, tittel: %s" % (
            self.id,
            self.klasse_ident,
            self.tittel,
        )


class Klassifikasjonssystem(NoarkEntity):
    tittel: Optional[str]
    beskrivelse: Optional[str]
    klassifikasjonstype: Optional[str]

    klasse_link = "refKlasse"
    arkivdel_som_primaer_link = "refArkivdelSomPrimaer"
    arkivdel_som_sekundaer_link = "refArkivdelSomSekundaer"

    def __str__(self):
        return "Id: %s, tittel: %s, beskrivelse: %s" % (
            self.id,
            self.tittel,
            self.beskrivelse,
        )


class Saksmappe(NoarkEntity):
    tittel: Optional[str]
    administrativ_enhet: str
    saksaar: Optional[int]
    sakssekvensnummer: Optional[str]
    saksansvarlig: Optional[str]
    saksansvarlig_brukerIdent: Optional[str]
    saksdato: Optional[Union[datetime.date, datetime.datetime]]
    saksstatus: Optional[Saksstatus]
    mappe_ident: Optional[str]
    dokumentmedium: Optional[str]
    prefiks: Optional[str]

    sakspart_link = "refSakspart"

    arkivdel_link = "refArkivdel"
    primaer_klasse_link = "refPrimaerKlasse"
    sekundaer_klasse_link = "refSekundaerKlasse"
    ekstern_id_link = "refEksternId"
    registrering_link = "refRegistrering"
    noekkelord_link = "refNoekkelord"
    merknad_link = "refMerknad"
    nasjonal_identifikator_link = "refNasjonalIdentifikator"
    kryssreferanse_til_mappe_link = "refKryssreferanseTilMappe"
    kryssreferanse_fra_mappe_link = "refKryssreferanseFraMappe"
    kryssreferanse_til_registrering_link = "refKryssreferanseTilRegistrering"
    kryssreferanse_fra_registrering_link = "refKryssreferanseFraRegistrering"


class Sakspart(NoarkEntity):
    sakspart_navn: str
    sakspart_rolle: str
    sakspart_ident: Optional[str]

    mappe_link = "refMappe"


class JournalpostType(str, Enum):
    inngaaende_dokument = "I"
    utgaaende_dokument = "U"
    organinternt_dokument_for_oppfoelging = "N"
    organinternt_dokument_uten_oppfoelging = "X"
    saksframlegg = "S"


class DokumentType(str, Enum):
    notat = "n_notat"
    vetak = "vedtak"
    inngaaende_kommentar = "inngaaende_kommentar"
    inngaaende_sak = "inngaaende_sak"
    saksframlegg = "saksframlegg"
    tilleggsdokumentasjon = "tilleggsdokumentasjon"


class Moteregistering(NoarkEntity):
    pass  # TODO


class Noekkelord(NoarkEntity):
    verdi: str

    mappe_link = "refMappe"
    registrering_link = "refRegistrering"


class Korrespondanseparttype(str, Enum):
    avsender = "EA"
    mottaker = "EM"
    kopimottaker = "EK"
    gruppemottaker = "GM"
    intern_avsender = "IA"
    intern_mottaker = "IM"
    intern_kopimottaker = "IK"


class FlytStatus(str, Enum):
    godkjent = "G"
    ikke_godkjent = "I"
    sendt_til_bake = "S"  # sendt tilbake til saksbehandler med kommentarer


class Dokumentflyt(NoarkEntity):
    flyt_fra: str
    flyt_fra_bruker_ident: str
    flyt_til: str
    flyt_til_bruker_ident: str
    flyt_mottatt_dato: datetime.datetime
    flyt_sendt_dato: datetime.datetime
    flyt_status: FlytStatus
    flyt_merknad: Optional[str]

    journalpost_link = "refJournalpost"

    # @validator('flyt_mottatt_dato', 'flyt_sendt_dato', pre=True)
    # def date_to_str(cls, v: datetime.datetime):
    #     return v.strftime("%Y-%M-%d")


class Basisregistrering(NoarkEntity):
    tittel: str
    offentlig_tittel: Optional[str]
    beskrivelse: Optional[str]
    avsluttet_dato: Optional[datetime.datetime]
    avsluttet_av: Optional[str]
    avsluttet_av_bruker_ident: Optional[str]
    forfatter: Optional[str]
    registrerings_dato: Optional[str]
    registrerings_ident: Optional[str]
    dokumentets_dato: Optional[str]
    forfallsdato: Optional[str]
    dokumentmedium: Optional[str]  # ref code-list: dokumentmedium
    kassasjonsvedtak: Optional[str]  # ref code-list: kassasjonsvedtak
    kassasjonshjemmel: Optional[str]  # ref code-list: kassasjonshjemmel
    skjerming: Optional[str]  # ref code-list: skjerming: Optional[str]
    virksomhetsspesifikke_metadata: Optional[str]

    mappe_link = "refMappe"
    ekstern_id_link = "refEksternId"
    arkivdel_link = "refArkivdel"
    primaer_klasse_link = "refPrimaerKlasse"
    sekundaer_klasse_link = "refSekundaerKlasse"
    korrespondansepart_link = "refKorrespondansepart"
    dokument_link = "refDokument"
    noekkelord_link = "refNoekkelord"
    merknad_link = "refMerknad"
    nasjonal_identifikator_link = "refNasjonalIdentifikator"
    kryssreferanse_til_mappe_link = "refKryssreferanseTilMappe"
    kryssreferanse_fra_mappe_link = "refKryssreferanseFraMappe"
    kryssreferanse_til_registrering_link = "refKryssreferanseTilRegistrering"
    kryssreferanse_fra_registrering_link = "refKryssreferanseFraRegistrering"


class Mappe(NoarkEntity):
    tittel: str
    offentlig_tittel: Optional[str]
    beskrivelse: Optional[str]
    skjerming: Optional[str]
    avsluttet_dato: Optional[datetime.datetime]
    avsluttet_av: Optional[str]
    avsluttet_av_bruker_ident: Optional[str]
    prefiks: Optional[str]
    mappe_ident: Optional[str]
    mappetype: Optional[str]
    dokumentmedium: Optional[str]  # ref code-list: dokumentmedium
    virksomhetsspesifikkeMetadata: Optional[str]

    arkivdel_link = "refArkivdel"
    primaer_klasse_link = "refPrimaerKlasse"
    sekundaer_klasse_link = "refSekundaerKlasse"
    ekstern_id_link = "refEksternId"
    registrering_link = "refRegistrering"
    noekkelord_link = "refNoekkelord"
    merknad_link = "refMerknad"
    nasjonal_identifikator_link = "refNasjonalIdentifikator"
    kryssreferanse_til_mappe_link = "refKryssreferanseTilMappe"
    kryssreferanse_fra_mappe_link = "refKryssreferanseFraMappe"
    kryssreferanse_til_registrering_link = "refKryssreferanseTilRegistrering"
    kryssreferanse_fra_registrering_link = "refKryssreferanseFraRegistrering"


class Korrespondansepart(NoarkEntity):
    korrespondansepart_navn: str
    administrativ_enhet: Optional[str]
    korrespondanseparttype: Korrespondanseparttype


class AvskrivingMaate(str, Enum):
    besvart_med_brev = "BU"
    besvart_med_epost = "BE"
    besvart_paa_telefon = "TLF"
    tatt_til_etterretning = "TE"
    tatt_til_orientering = "TO"


class Avskrivning(NoarkEntity):
    avskrivningsmaate: AvskrivingMaate

    journalpost_link = "refJournalpost"
    ref_tilknyttet_journalpost_link = "refTilknyttetJournalpost"


class Person(BaseModel):
    navn: str
    bruker_ident: str


class Dokumentmedium(str, Enum):
    elektronisk_arkiv = "E"


class JournalpostStatus(str, Enum):
    journalfoert = "J"
    ferdigstilt_saksbehandler = "F"  # Ferdigstilt fra saksbehandler
    godkjent_av_leder = "G"
    ekspedert = "E"
    arkivert = "A"
    utgaar = "U"  #
    temp_registed = "M"  # Midlertidigregistrering av innkommet dokument
    reg_in_dok = "S"  # Saksbehandler har registrert innkommet dokument
    reservert = "R"  # Reservert dokument


class Journalpost(AbstractRegistering, NoarkEntity):
    tittel: Optional[str]
    offentlig_tittel: Optional[str]
    beskrivelse: Optional[str]
    forfatter: Optional[str]
    journalaar: Optional[int]
    journalsekvensnummer: Optional[int]
    journalpostnummer: Optional[int]
    journalansvarlig: Optional[str]
    journalansvarlig_brukerIdent: Optional[str]

    dokumentets_dato: Optional[datetime.datetime]  # the resp is in timestamp format
    mottatt_dato: Optional[datetime.datetime]  # the resp is in timestamp format
    sendt_dato: Optional[datetime.datetime]  # the resp is in timestamp format
    forfallsdato: Optional[datetime.datetime]  # the resp is in timestamp format
    offentlighetsvurdert_dato: Optional[datetime.datetime]

    skjerm_korrespondanse_parter_e_innsyn: Optional[bool]

    dokumentmedium: Optional[Dokumentmedium]
    skjerming: Optional[Skjerming]

    journalposttype: JournalpostType
    journalstatus: Optional[JournalpostStatus]


class TilknyttetRegistreringSom(str, Enum):
    hoveddokument = "H"
    vedlegg = "V"


class DokumentStatus(str, Enum):
    dokumentet_er_under_redigering = "B"
    dokumentet_er_ferdigstilt = "F"


class Dokument(NoarkEntity):
    tittel: str
    dokumenttype: Optional[str]
    beskrivelse: Optional[str]
    forfatter: Optional[str]
    dokumentnummer: Optional[int]
    tilknyttet_registrering_som: TilknyttetRegistreringSom
    dokumentstatus: Optional[DokumentStatus]

    ref_registrering_link = "refRegistrering"


class Variantformat(str, Enum):
    produksjonsformat = "P"
    arkivformat = "A"


class Dokumentversjon(NoarkEntity):
    format: str
    variantformat: Variantformat
    referanse_dokumentfil: str  # referanse til opprettede dokument
    filnavn: Optional[str]
    kryptert_dokument: Optional[bool]
    filstoerrelse: Optional[int]
    sjekksumAlgoritme: Optional[str]
    sjekksum: Optional[str]
    versjonsnummer: Optional[int]
    format_detaljer: Optional[str]

    dokument_link = "refDokument"


class EksternId(NoarkEntity):
    eksternt_system: str
    ekstern_id: str = Field(alias="eksternID")


class UnsuccessfulRequestException(Exception):
    def __init__(self, url: str, msg: str):
        self.url = url
        self.msg = msg


class InvalidLink(Exception):
    def __init__(self, src: str, field: str, to: str, msg: str):
        self.src = src
        self.field = field
        self.to = to
        self.msg = msg


class ClientError(Exception):
    pass  # ok


class Doc(BaseModel):
    name: str
    content: Optional[bytes]
    title: str
    id: Optional[str]  # documaster uploaded id
    stamp: Optional[
        datetime.datetime
    ]  # stamp on when the file was uploaded. Documaster have one hour to be linked the document


class VirksomhetsspesifikkeMetadata(BaseModel):
    pass


model_by_type = {
    "Arkivskaper": Arkivskaper,
    "Arkivdel": Arkivdel,
    "Arkiv": Arkiv,
    "Avskrivning": Avskrivning,
    "Klasse": Klasse,
    "Klassifikasjonssystem": Klassifikasjonssystem,
    "Mappe": Mappe,
    "Saksmappe": Saksmappe,
    "Journalpost": Journalpost,
    "Dokumentflyt": Dokumentflyt,
    "Dokument": Dokument,
    "Dokumentversjon": Dokumentversjon,
    "Basisregistrering": Basisregistrering,
    "Sakspart": Sakspart,
    "Korrespondansepart": Korrespondansepart,
    "EksternId": EksternId,
}


class Stats(BaseModel):
    mem_rss: Any
    cpu_perc: Any
    mem_perc: Any
    mem_full: Any
    time: float

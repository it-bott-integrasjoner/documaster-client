import logging
import random
from typing import Any, List, Dict

import time

logger = logging.getLogger(__name__)


def get_unq_nr():
    """Get a unique nr to use as sekvensnr in sak or jp, this is required to save entities in parallel"""
    r = random.randint(0, 100000)
    sekv_nr = int(time.time())
    return int(sekv_nr) + r


def chop_list(entities: List[Any], n: int) -> List[Any]:
    result: List[Any] = []
    if len(entities) < n:
        result.append(entities[0 : len(entities)])
        return result
    rest = len(entities) % n
    factor = int(len(entities) / n)
    for i in range(0, factor):
        result.append(entities[i * n : (i * n) + n])
    max_nr = factor * n
    if max_nr > 0 and rest != 0:
        result.append(entities[max_nr : max_nr + rest])
    return result


def validate_add_unq_id(entity: Any, unq_link: Dict[str, str]) -> None:
    """Validate: The entities should have unique id"""
    if entity.id is None:
        raise ValueError(
            f"Invalid entity: {str(entity)}, missing id (every should have id or tmp id"
        )
    key = type(entity).__name__ + "-" + entity.id
    if unq_link.get(key):
        raise ValueError(f"Duplicate entities with key: {entity.id}")
    unq_link[key] = entity.id
    logger.debug(f"Added unique id: {key}, len={len(unq_link)}")


def to_lower_camel(s: str) -> str:
    first, *others = s.split("_")
    return "".join([first.lower(), *map(str.capitalize, others)])


def camel_to_snake(s: str) -> str:
    if s == "eksternID":
        return "ekstern_id"
    return "".join(["_" + c.lower() if c.isupper() else c for c in s]).lstrip("_")


def to_upper_camel(s: str) -> str:
    """Alias generator to avoid breaking PEP8"""
    parts = s.split("_")
    return "".join(map(str.capitalize, parts))

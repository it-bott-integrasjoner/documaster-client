import functools
import os
from typing import Optional, List, Iterator, Union

import sys

from documaster_client.model import DocumasterClientConfigLoader, DocumasterClientConfig

FILES_ETC = os.path.join("" if sys.prefix == "/usr" else sys.prefix, "etc")

SERVICE_NAME = __name__.split(".")[0]

CONFIG_NAME = "config.yaml"

CONFIG_LOOKUP_ORDER = [
    os.getcwd(),
    os.path.expanduser(f"~/{SERVICE_NAME}"),
    os.path.join(FILES_ETC, f"{SERVICE_NAME}"),
]
CONFIG_ENVIRON = f"{SERVICE_NAME.upper()}_CONFIG"


def iter_config_locations(
    filename: str = CONFIG_NAME, lookup_order: Optional[List[str]] = None
) -> Iterator[str]:
    if CONFIG_ENVIRON in os.environ:
        yield os.path.abspath(os.environ[CONFIG_ENVIRON])
    for path in lookup_order or CONFIG_LOOKUP_ORDER:
        yield os.path.abspath(os.path.join(path, filename))


def get_config(filename: str = str(None)) -> DocumasterClientConfig:

    return get_config_loader(filename).documaster_client


@functools.lru_cache()
def get_config_loader(filename: Union[str, None] = None) -> DocumasterClientConfigLoader:
    if filename is None:
        for filename in filter(os.path.exists, iter_config_locations()):
            break
    if filename:
        return DocumasterClientConfigLoader.from_file(filename)
    raise ValueError(f"No config file found")

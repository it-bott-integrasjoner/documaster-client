from typing import Union, Dict, Optional, List, Any

from documaster_client.client import DocumasterClient
from documaster_client.model import (
    Arkivskaper,
    SortOrder,
    Arkiv,
    Arkivdel,
    ValueOperator,
    Oper,
    CompOper,
    Klassifikasjonssystem,
    Klasse,
    NoarkEntity,
)


def get_arkivskaper(client: DocumasterClient, ask_ident: str):
    return client.do_query(
        Arkivskaper,
        fields={"arkivskaperIdent": ask_ident},
        sort=SortOrder(field="opprettetDato", order="desc"),
    )


def get_arkiver(
    client: DocumasterClient,
    arkivskaper_ident: str,
    arkivskaper_id: str = None,
    id: Optional[str] = None,
):
    fields = {"refArkivskaper.arkivskaperIdent": arkivskaper_ident}
    fields = add_id(fields, id)
    if arkivskaper_id:
        fields.update({"refArkivskaper.id": arkivskaper_id})

    return client.do_query(Arkiv, fields=fields)


def add_id(
    fields: Dict[str, Union[ValueOperator, str]], id: Optional[str]
) -> Dict[str, Union[ValueOperator, str]]:
    if id:
        fields.update(
            {
                "id": ValueOperator(
                    field_value=id, oper=Oper.AND, comp_oper=CompOper.LIKE
                ),
            }
        )
    return fields


def get_arkivdeler(client: DocumasterClient, arkiv_saved=None, id: str = None):
    fields = {}
    if arkiv_saved:
        fields.update({"refArkiv.id": arkiv_saved.id})
    fields = add_id(fields, id)
    try:
        result = client.do_query(
            Arkivdel,
            fields=fields,
        )
    except Exception as err:
        if id:
            raise ValueError("Could not find arkiv with id: %", id)
        raise ValueError("Could not find arkiv", err)
    return result


def get_klassesystem_entity(
    client: DocumasterClient,
    arkivdel=None,
    id: str = None,
    only_primaer=False,
    only_sekundaer=False,
) -> List[Klassifikasjonssystem]:
    fields = {}
    if not arkivdel and (only_primaer or not only_sekundaer):
        raise ValueError("Missing Arkivdel, either onlyprimary or only.secondary")
    if only_primaer and only_sekundaer:
        raise ValueError("Invalid param, cant get klasse_system")

    if arkivdel:
        if only_primaer:
            fields["refArkivdelSomPrimaer.id"] = ValueOperator(
                field_value=arkivdel.id, comp_oper=CompOper.LIKE
            )
        if only_sekundaer:
            fields["refArkivdelSomSekundaer.id"] = (
                ValueOperator(field_value=arkivdel.id, comp_oper=CompOper.LIKE),
            )
    if id:
        add_id(fields, id)

    kls_entities = client.do_query(
        Klassifikasjonssystem,
        fields=fields,
    )
    return kls_entities


def get_by_id(client: DocumasterClient, cls: Any, id: str) -> Optional[NoarkEntity]:
    fields = {}
    add_id(fields, id)

    return client.do_query(cls, fields=fields, exp_one_result=True)


def get_klasser(
    client: DocumasterClient,
    kls: Union[NoarkEntity, Klassifikasjonssystem] = None,
    ident: str = None,
    id: str = None,
) -> List[Klasse]:
    fields = {}
    if kls:
        fields.update(
            {
                "refKlassifikasjonssystem.id": ValueOperator(
                    field_value=kls.id, oper=Oper.AND, comp_oper=CompOper.LIKE
                )
            }
        )
    if ident:
        fields.update(
            {
                "klasseIdent": ValueOperator(
                    field_value=ident, oper=Oper.AND, comp_oper=CompOper.LIKE
                ),
            }
        )
    if id:
        fields = add_id(fields, id)
    klasser = client.do_query(
        Klasse,
        fields=fields,
    )
    return klasser


def get_klasse_w_prefix(
    client: DocumasterClient, kls_id: str, prefix: str
) -> Union[Klasse, None]:
    fields = {
        "refKlassifikasjonssystem.id": ValueOperator(
            field_value=kls_id, comp_oper=CompOper.LIKE
        ),
        "klasseIdent": ValueOperator(
            field_value=f"%{prefix}", oper=Oper.AND, comp_oper=CompOper.WLIKE
        ),
    }
    return client.do_query(cls=Klasse, fields=fields, exp_one_result=True)

import datetime
import logging
import threading
from typing import List, Dict, Optional, Union, Any, Type

import requests
from pydantic import create_model
from time import sleep
from uplink import (
    post,
    Consumer,
    Body,
    response_handler,
    returns,
    json,
    Path,
    put,
    get,
    QueryMap,
    Header,
    multipart,
    Part,
    headers,
)

from documaster_client.model import (
    DocumasterClientConfig,
    TransactionRequest,
    UnsuccessfulRequestException,
    NoarkEntity,
    SaveAction,
    LinkAction,
    TransactionResp,
    QueryRequest,
    QueryResp,
    NoarkCode,
    CodeListRespHolder,
    SortOrder,
    Doc,
    ClientError,
    model_by_type,
    ValueOperator,
)
from documaster_client.util import (
    validate_add_unq_id,
    chop_list,
    camel_to_snake,
)

logger = logging.getLogger(__file__)

_upload_headers = {"Content-Type": "application/octet-stream"}

CHOP_SIZE = 100

BODY_DICT: Body = Body(type=Dict)

BODY_TRANS_REQ: Body = Body(type=TransactionRequest)

HEADER_CONTENT_DISP: Header = Header("Content-Disposition")

PATH_FIELD: Path = Path("field")

PATH_CODE: Path = Path("code")

# type of noark entities
NoarkEntityType = Type


def raise_for_status(response: requests.models.Response) -> requests.models.Response:
    """Checks whether the response was successful."""
    if 200 <= response.status_code < 300:
        # Pass through the response.
        return response
    msg = "Error response: %s, msg: %s", str(response), response.text
    logger.error(msg)
    raise UnsuccessfulRequestException(response.url, response.text)


def get_base_url(url: str) -> str:
    base_url = url
    if not url.endswith("/"):
        base_url = url + "/"
    return base_url


def get_operator(val: Any) -> str:
    val_type = type(val)
    if val_type == str:
        return " && "
    elif val_type == ValueOperator:
        return f" {val.oper} "
    else:
        raise ValueError(f"Invalid operator, unexpected type: {val_type}")


def get_value(val: Any) -> Any:
    val_type = type(val)
    if val_type == str:
        return f"{val}"
    elif val_type == ValueOperator:
        if type(val.field_value) == str:
            return f"{val.field_value}"
        return val.field_value
    else:
        raise ValueError(f"Invalid value, unexpected type: {val_type}")


def get_param_name(field: str, prefix: Union[str, None] = None) -> str:
    """No special characters are supported"""
    if not prefix:
        prefix = ""
    param_name = f"@{prefix}{field}".replace(".", "").replace("#", "")
    return param_name


def get_comp_oper(val: Any) -> Union[str, None]:
    val_type = type(val)
    if val_type == str:
        return f"="
    elif val_type == ValueOperator:
        return f"{val.comp_oper}"
    return None


class DocumasterClient(Consumer):
    def __init__(self, config: DocumasterClientConfig, **kwargs: Any):
        super(DocumasterClient, self).__init__(base_url=get_base_url(config.url))
        self.cache_code_list: Dict[str, NoarkCode] = {}
        self.config = config
        self.session.headers.update(config.headers)
        self.lock = threading.Lock()
        if self.config.code_list.cache:
            self.refresh_cache()

    @json
    @returns.json
    @response_handler(raise_for_status)
    @post("transaction")
    def _post_transaction(self, transaction_req: BODY_TRANS_REQ) -> None:
        pass

    @json
    @returns.json
    @response_handler(raise_for_status)
    @post("query")
    def _post_query(self, query_req: BODY_DICT) -> None:
        pass

    @json
    @returns.json
    @response_handler(raise_for_status)
    @put("code-lists/{field}/{code}")
    def _put_code_list(
        self, field: PATH_FIELD, code: PATH_CODE, code_list_req: BODY_DICT
    ) -> None:
        pass

    @json
    @returns.json
    @response_handler(raise_for_status)
    @get("code-lists")
    def _get_code_list(self, **params: QueryMap) -> None:
        pass

    @response_handler(raise_for_status)
    @get("download")
    def _get_download(self, **params: QueryMap) -> None:
        pass

    @returns.json
    @multipart
    @response_handler(raise_for_status)
    @headers(_upload_headers)
    @post("upload")
    def _post_upload(self, dc_header: HEADER_CONTENT_DISP, document: Part) -> None:
        pass

    def download_file(self, file_id: str) -> Any:
        return self._get_download(**{"id": file_id})

    def get_code_list(self, field: str, type: Optional[str] = None) -> Any:
        arg = {"field": field}
        if type:
            arg["type"] = type
        resp = self._get_code_list(**{"field": field})

        return CodeListRespHolder(**resp)

    def save_code_list(self, field: str, code_list: NoarkCode) -> Any:
        adict = code_list.dict(exclude={"code"})

        return self._put_code_list(
            field=field, code=code_list.code, code_list_req=adict
        )

    def do_query(
        self,
        cls: NoarkEntityType,
        fields: Dict[str, Union[str, ValueOperator]],
        exp_one_result=False,
        sort=None,
    ):
        ent_name = cls.__name__
        if ent_name not in model_by_type.keys():
            raise ValueError(
                f"Invalid noark entity name: {ent_name}, must one of: {model_by_type.keys()}"
            )

        a_query = self.new_query(ent_name, fields)
        result = self.query(a_query, cls)

        for a in result:
            logger.debug(a)
        logger.debug(f"Query, found: entity:{ent_name}, len: {len(result)}")

        if not exp_one_result:
            ret = []
            [ret.append(a) for a in result]
            return ret
        # Expect one singe entity or raise value error
        if not result:
            raise ValueError(f"Expected 1 entity of <{ent_name}> but found none")
        if len(result) > 1:
            raise ValueError(
                f"Expected 1 entity of <{ent_name}> but found many: {len(result)}"
            )
        return result[0]

    def query(
        self, query_request: QueryRequest, cls: Any = None
    ) -> Union[Dict[str, NoarkEntity], List[NoarkEntity]]:
        logger.debug("Executing query: %s", query_request.query)
        resp = self._post_query(query_request.dict(by_alias=True, exclude_none=False))
        logger.debug("Got query resp:  %s", resp)
        query_resp = QueryResp(**resp)
        result: Dict[Any, Any] = {}
        for obj in query_resp.results:
            self.create_from_resp(result, obj)

        if cls:
            return list(result.values())
        return result

    def upload_documents(self, docs: List[Doc]) -> List[Doc]:
        result = []
        for idx, doc in enumerate(docs):
            logger.debug("Uploading doc: %s", str(idx))

            resp = self._post_upload(
                dc_header=self.get_dc_header(doc), document=doc.content
            )
            if resp:
                doc.id = resp.get("id")
                doc.stamp = datetime.datetime.now()
                logger.debug("Uploaded doc: %s, id: %s", doc.name, doc.id)
                result.append(doc)

            if not doc.id:
                ClientError(f"Could not upload document name: {doc.name} Resp: {resp}")
        return result

    def bulk_transaction_call(
        self,
        entities: List[NoarkEntity],
        saved: Union[Dict[str, NoarkEntity], None] = None,
        chop: bool = True,
        retry: bool = True,
    ) -> Dict[str, NoarkEntity]:
        if saved is None:
            saved = {}
        if type(entities) is not list:
            raise ValueError(f"Required list, but found: {type(entities)}")
        if chop:
            chopped_list = chop_list(entities, CHOP_SIZE)
        else:
            chopped_list = [entities]

        for li in chopped_list:
            if type(li) is not list:
                raise ValueError("Expected list but got: %s", type(li))
            req = self.transaction_req(li, saved)

            logger.debug("Making: POST transaction req, actions: %s", len(req.actions))
            resp = None
            retry_count = 0
            while retry and retry_count < self.config.retry_max or not retry:
                try:
                    resp = self._post_transaction(req)
                    break
                except Exception as ea:
                    logger.error(
                        "Got transaction error-response, req: %s",
                        req.json(exclude_unset=True),
                    )
                    if not retry:
                        return saved
                    logger.error(
                        "Retry count: %s/%s",
                        retry_count,
                        self.config.retry_max,
                        exc_info=True,
                    )
                    retry_count += 1
                    sleep(2)

            if resp is None:
                raise ValueError("Got error when posting transaction, after retry..")

            now_saved = self.create_saved_objects(TransactionResp(**resp))

            saved.update(now_saved)

        return saved

    def transaction_req(
        self,
        entities: List[NoarkEntity],
        saved: Union[Dict[str, NoarkEntity], None] = None,
    ) -> TransactionRequest:
        if not entities:
            raise ValueError("Cant create transaction req, empty entities")
        action_list = []
        link_list: List[Any] = []
        unq_link: Dict[Any, Any] = {}
        for entity in entities:
            type_name = type(entity).__name__
            validate_add_unq_id(entity, unq_link)
            sa = SaveAction(
                type=type_name,
                id=entity.id,
                fields=entity.dict(
                    by_alias=True,
                    exclude_none=True,
                    exclude_unset=True,
                    exclude={"links": True, "id": True},
                ),
            )
            action_list.append(sa)

            for link_key in entity.links.keys():
                link_entity = entity.links[link_key]

                if type(link_entity) == list:
                    link_entity_list = link_entity
                else:
                    link_entity_list = [link_entity]
                for link_ent in link_entity_list:
                    self.create_link_action(
                        entity, link_ent, link_key, link_list, saved, type_name
                    )

        return TransactionRequest(actions=action_list + link_list)

    def update_cached_code_list(self, noark: NoarkCode):
        lock = threading.Lock()
        try:
            lock.acquire()
            noark.code = noark.code
            self.cache_code_list[noark.code] = noark
        except Exception as se:
            logger.error(f"Could not refresh code-list cache, {se}")
        finally:
            lock.release()
        logger.debug("Updated code-list cache: %s", self.cache_code_list.keys())

    def refresh_cache(self):
        """Cache code list"""
        if not self.config.code_list.cache:
            logger.info("Code list cache id disabled,")
            return
        # default code list type = Saksmappe
        code_list_type = (
            self.config.code_list.type if self.config.code_list.type else "Saksmappe"
        )
        cf = {}
        for field_name in self.config.code_list.field_names:
            resp = self.get_code_list(field=field_name, type=code_list_type)
            if not resp.results:
                raise ValueError(f"Empty resp, could not refresh code list cache")
            for noark_code in resp.results[0].values:
                cf[noark_code.code] = noark_code
        try:
            self.lock.acquire()
            self.cache_code_list = cf
        except Exception as se:
            logger.error(f"Could not refresh code-list cache, {se}")
        finally:
            self.lock.release()

        logger.info("Refreshed code-list cache: %s", len(self.cache_code_list))
        if logger.isEnabledFor(logging.DEBUG):
            for nc in self.cache_code_list.keys():
                logger.debug("Code: %s, Name: %s", nc, self.cache_code_list[nc].name)

    @staticmethod
    def create_link_action(
        entity: Any,
        link_entity: Any,
        link_key: Any,
        link_list: List[Any],
        saved: Any,
        type_name: str,
    ) -> None:
        saved_obj = saved and saved.get(link_entity)
        link_to_id = saved_obj.id if saved and saved_obj else link_entity
        la = LinkAction(
            id=entity.id,
            type=type_name,
            ref=link_key,
            linkToId=link_to_id,
        )
        link_list.append(la)

    @staticmethod
    def get_param_name(field: Any) -> None:
        """Parameter name does not support special characters"""
        pass

    @staticmethod
    def new_query(
        cls: str,
        fields: Dict[str, Union[str, ValueOperator]],
        joins: Union[Dict[str, str], None] = None,
        sort: SortOrder = None,
        limit: int = 500,  # default max limit
    ) -> QueryRequest:
        """Builds a query object that can be used on query methods.
        NB: Documaster does not support special characters (and underscore) in parameter names
        """
        logger.debug(f"Query fields keys:{str(fields.keys())}")
        params = {}
        query: Any = ""
        count = 0
        for field, val in fields.items():
            if not val:
                continue
            if count > 0:
                query += get_operator(val)
            param_name = get_param_name(field, "param")
            params[param_name] = get_value(val)
            comp_oper = get_comp_oper(val)
            # field = param_name
            query += f"{field} {comp_oper} {param_name}"
            count += 1
        if joins:
            for rel_key, rel_val in joins.items():
                if not rel_key.startswith("#"):
                    raise ValueError("Invalid join query, joins starts with #")
                if rel_key not in query:
                    raise ValueError(
                        f"Invalid join query, query missing rel key: {rel_key}"
                    )
        query = QueryRequest(
            type=cls,
            query=query.strip(),
            parameters=params,
            joins=joins,
            limit=limit,
            sort_order=sort,
        )
        logger.debug("query: %s", query.dict(by_alias=True, exclude_none=True))
        return query

    @staticmethod
    def create_saved_objects(
        transaction_resp: TransactionResp,
    ) -> Dict[str, NoarkEntity]:
        result: Dict[Any, Any] = {}
        for tmp_id, saved_item in transaction_resp.saved.items():
            try:
                DocumasterClient.create_from_resp(result, saved_item, tmp_id)
            except Exception as e:
                logger.error(
                    f"Cant create object for id: {tmp_id}, error: {e}",
                    exc_info=True,
                )
                raise e
        return result

    @staticmethod
    def create_from_resp(
        result: Dict[str, Any], resp_item: Any, tmp_id: Union[str, None] = None
    ) -> None:
        """Creates noark5 entity"""
        model: Any = create_model(
            resp_item.type,
            __config__=NoarkEntity.__config__,
        )

        model._tmp_id = tmp_id
        model.id = resp_item.id
        model.version = resp_item.version
        for field, val in resp_item.fields.items():
            field_name = camel_to_snake(field)
            setattr(model, field_name, val)
        key = tmp_id or model.id

        if resp_item.links:
            links = {}
            for li_name, li_val in resp_item.links.items():
                field_name = camel_to_snake(li_name)
                links.update({field_name: li_val})
            model.links = links
        logger.debug(
            "Converted object from resp:  Type: %s, id: %s", model.__name__, model.id
        )
        mode_type = model_by_type.get(model.__name__)
        if not mode_type:
            raise ValueError(
                f"Noark type is not defined in models, required to add type: {mode_type}"
            )
        result[key] = mode_type.from_orm(model)

    @staticmethod
    def get_dc_header(doc: Doc) -> str:
        valid_files = ["pdf", "txt", "text"]
        for vf in valid_files:
            if vf in doc.name:
                logger.debug(f"Uploading, filename: {doc.name}")
                return f'attachment; FILENAME="{doc.name}"'

        raise ValueError(
            f"Cant get document composition, only pdf, invalid doc: {doc.name}"
        )

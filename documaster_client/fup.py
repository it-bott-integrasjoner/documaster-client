import os
from typing import List, Any, Set, Union

from documaster_client.model import FUPItem


class FUP:
    def __init__(self, csv_file: str):
        self.fup_items: List[FUPItem] = []
        self.funksjoner: Set[str] = set()
        self.under_funksjoner: Set[str] = set()
        self.prosesser: Set[str] = set()
        self.read_csv_data(csv_file)

    def read_csv_data(self, csv_file: str) -> None:
        here = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
        with open(os.path.join(here, csv_file)) as f:
            lines = f.readlines()
            self.fup_items = self.create_fup_items(lines)

    def create_fup_items(self, lines: List[str]) -> List[FUPItem]:
        fup_items = []
        for line in lines:
            cols = [x.strip() for x in line.replace("\n", "").split(",")]
            fup_items.append(
                FUPItem(
                    funksjon=cols[0],
                    under_funksjon=cols[1],
                    prosess=cols[2],
                )
            )
            self.funksjoner.add(cols[0])
            self.under_funksjoner.add(cols[1])
            self.prosesser.add(cols[2])
        return fup_items

    def get_funksjoner_by_underfunksjoner(
        self, uf: str, replace_space: Union[str, Any] = None
    ) -> Any:
        for item in self.fup_items:
            if item.under_funksjon == uf:
                return (
                    item.funksjon.lower().replace(" ", replace_space)
                    if replace_space
                    else item.funksjon
                )

        raise ValueError(f"Could not find funksjoner for underfunksjon: {uf}")

import datetime
from typing import Optional, List, Any, Union, Dict

from documaster_client.model import (
    Arkivskaper,
    Arkiv,
    Arkivdel,
    Klassifikasjonssystem,
    NoarkEntity,
    Saksmappe,
    Klasse,
    Journalpost,
    Noekkelord,
    InvalidLink,
    JournalpostType,
    Sakspart,
    Korrespondansepart,
    Doc,
    Dokumentversjon,
    Variantformat,
    Dokument,
    TilknyttetRegistreringSom,
    EksternId,
    Dokumentflyt,
    Person,
    Mappe,
    Basisregistrering,
    DokumentStatus,
    Korrespondanseparttype,
    FlytStatus,
)


def create_arkiv_skaper(
    aid: Union[str, None], ident: Union[str, None], navn: Union[str, None]
) -> Arkivskaper:
    return Arkivskaper(id=aid, arkivskaper_ident=ident, arkivskaper_navn=navn)


def create_arkiv(
    aid: Union[str, None], tittel: str, arkivskaper: Any, besk: Union[str, None] = None
) -> Arkiv:
    arkiv = Arkiv(id=aid, tittel=tittel, beskrivelse=besk)
    arkiv.links[arkiv.arkivskaper_link] = arkivskaper.id
    return arkiv


def create_arkiv_del(
    aid: Union[str, None],
    tittel: str,
    arkiv: Arkiv,
    besk: Union[str, None] = None,
    prim_klsystem: Klassifikasjonssystem = None,
    sek_klsystem: Klassifikasjonssystem = None,
    saksmappe: Saksmappe = None,
    **kwargs: Any,
) -> Arkivdel:
    validate_id(arkiv)

    arkivdel = Arkivdel(
        id=aid,
        tittel=tittel,
        beskrivelse=besk,
        **kwargs,
    )

    add_link(arkivdel, arkivdel.primaer_klassifikasjonssystem_link, prim_klsystem)
    add_link(arkivdel, arkivdel.sekundaer_klassifikasjonssystem_link, sek_klsystem)
    add_link(arkivdel, arkivdel.mappe_link, saksmappe)
    add_link(arkivdel, arkivdel.arkiv_link, arkiv)

    # add_link(arkivdel, arkivdel.registrering_link, sek_klsystem)
    return arkivdel


def create_noekkelord(
    verdi: str, saksmappe: Saksmappe, noark_entity: NoarkEntity
) -> Noekkelord:
    ord = Noekkelord(verdi=verdi)
    add_link(ord, ord.mappe_link, saksmappe)
    add_link(ord, ord.registrering_link, noark_entity)
    return ord


def create_sakspart(
    aid: Union[str, None], sakspart_navn: str, sakspart_rolle: Union[str, None]
) -> Sakspart:
    return Sakspart(id=aid, sakspart_navn=sakspart_navn, sakspart_rolle=sakspart_rolle)


def create_korrespondansepart(
    aid: Union[str, None], navn: str, adm_enhet, korr_type: Korrespondanseparttype
) -> Korrespondansepart:
    return Korrespondansepart(
        id=aid,
        korrespondansepart_navn=navn,
        administrativ_enhet=adm_enhet,
        korrespondanseparttype=korr_type,
    )


def create_saksmappe(
    aid: Union[str, None],
    tittel: str,
    admin_enhet: str,
    arkivdel: Arkivdel,
    prefiks: Optional[str] = None,
    sekvnr: Union[str, None] = None,
    sakspart: Optional[Sakspart] = None,
    prim_klasse: Optional[Klasse] = None,
    sek_klasse: Optional[Klasse] = None,
    mappe_ident: Union[str, None] = None,
) -> Saksmappe:
    saksmappe = Saksmappe(
        id=aid,
        tittel=tittel,
        administrativ_enhet=admin_enhet,
        mappe_ident=mappe_ident,
        sakssekvensnummer=sekvnr,
        prefiks=prefiks,
    )

    add_link(saksmappe, saksmappe.arkivdel_link, arkivdel)
    add_link(saksmappe, saksmappe.sakspart_link, sakspart)
    add_link(saksmappe, saksmappe.primaer_klasse_link, prim_klasse)
    add_link(saksmappe, saksmappe.sekundaer_klasse_link, sek_klasse)

    return saksmappe


def create_ekstern_id(aid: Union[str, None], eks_id: str, eks_system: str) -> EksternId:
    return EksternId(id=aid, ekstern_id=eks_id, eksternt_system=eks_system)


def create_jp(
    aid: Union[str, None],
    tittel: str,
    saksmappe: Saksmappe,
    korrespndanseparter: List[Korrespondansepart],
    jp_type: JournalpostType,
    beskr: Union[str, None] = None,
    ekstern: EksternId = None,
    jp_aar: Union[int, None] = None,
    jp_sekvnr: Union[int, None] = None,
    noekkelord: Any = None,
) -> Journalpost:
    jp = Journalpost(
        id=aid,
        tittel=tittel,
        beskrivelse=beskr,
        journalposttype=jp_type,
        journalaar=jp_aar,
        journalsekvensnummer=jp_sekvnr,
    )

    add_link(jp, jp.mappe_link, saksmappe)
    add_link(jp, jp.noekkelord_link, noekkelord)
    add_link(jp, jp.ekstern_id_link, ekstern)
    add_link_list(jp, jp.korrespondansepart_link, korrespndanseparter)

    return jp


def create_dokument_version(
    jp: Journalpost,
    dokument_type: str,
    dokument_status: DokumentStatus,
    docs: List[Doc],
    upload: bool = False,
) -> List[NoarkEntity]:
    if jp.id is None:
        raise ValueError(
            f"Cant upload docs to jp: {jp.tittel}, missing id, should be saved"
        )
    entities: List[Any] = []
    tilknyttet = TilknyttetRegistreringSom.hoveddokument
    for idx, uploaded_doc in enumerate(docs):
        if idx > 0:
            tilknyttet = TilknyttetRegistreringSom.vedlegg

        dok = Dokument(
            id=f"dok-jp-{jp.id}-{idx}",
            tittel=uploaded_doc.title,
            dokumenttype=dokument_type,
            tilknyttet_registrering_som=tilknyttet,
            dokumentstatus=dokument_status,
        )
        add_link(dok, dok.ref_registrering_link, jp)
        entities.append(dok)

        if upload:
            validate_doc(jp, uploaded_doc)
            dok_ver = Dokumentversjon(
                id=f"dok-vers-jp-{jp.id}-{idx}",
                variantformat=Variantformat.produksjonsformat,
                format="application/pdf",
                referanse_dokumentfil=uploaded_doc.id,
            )
            add_link(dok_ver, dok_ver.dokument_link, dok)
            entities.append(dok_ver)
            print(
                "Created document version with jp_id: %s doc_id: %s"
                % (jp.id, uploaded_doc.id)
            )

    return entities


def validate_doc(jp: Journalpost, uploaded_doc: Doc) -> None:
    if not uploaded_doc.content:
        raise ValueError("Invalid doc to upload, content is empty")
    if uploaded_doc.id is None:
        raise ValueError(f"Cant upload docs to jp.id: {jp.id}, doc.id is None")


def create_klassifikasjonssystem(
    aid: Union[str, None],
    tittel: str,
    besk: Union[str, None] = None,
    klstype: Union[str, None] = None,
    akrivdel_prim: Any = None,
    akrivdel_sek: Any = None,
    klasse: Any = None,
) -> Klassifikasjonssystem:
    kls = Klassifikasjonssystem(
        id=aid, tittel=tittel, beskrivelse=besk, klassifikasjonstype=klstype
    )
    add_link(kls, kls.arkivdel_som_primaer_link, akrivdel_prim)
    add_link(kls, kls.arkivdel_som_sekundaer_link, akrivdel_sek)
    add_link(kls, kls.klasse_link, klasse)

    return kls


def create_klassering(
    aid: Union[str, None],
    ident: str,
    tittel: str,
    klsystem: Klassifikasjonssystem = None,
    prim_mappe: Any = None,
    sek_mappe: Any = None,
) -> Klasse:
    kl = Klasse(id=aid, klasse_ident=ident, tittel=tittel)

    add_link(kl, kl.klassifikasjonssystem_link, klsystem)
    add_link(kl, kl.mappe_link_som_primaer, prim_mappe)
    add_link(kl, kl.mappe_link_som_sekundaer, sek_mappe)
    # TODO Check if we need to create a registering link also
    # add_link(kl, kl.registrering_som_primaer_link, sek_mappe)
    # add_link(kl, kl.registrering_som_sekundaer_link, sek_mappe)

    return kl


def create_basis_reg(
    aid: Union[str, None], tittel: str, mappe: Mappe
) -> Basisregistrering:
    basis_reg = Basisregistrering(id=aid, tittel=tittel)
    add_link(basis_reg, basis_reg.mappe_link, mappe)
    return basis_reg


def create_mappe(
    aid: Union[str, None],
    tittel: str,
    arkivdel: Arkivdel,
    prim_klasse: Klasse = None,
    sek_klasse: Klasse = None,
) -> Mappe:
    mappe = Mappe(id=aid, tittel=tittel)

    add_link(mappe, mappe.arkivdel_link, arkivdel)
    add_link(mappe, mappe.primaer_klasse_link, prim_klasse)
    add_link(mappe, mappe.sekundaer_klasse_link, sek_klasse)

    return mappe


def create_dokument_flyt(
    jp_saved: Journalpost,
    fra_person: Person,
    til_person: Person,
    mottatt_dato: datetime.datetime,
    sendt_dato: datetime.datetime,
    status: FlytStatus,
) -> Dokumentflyt:
    df = Dokumentflyt(
        id="dokument-flyt-jp-" + jp_saved.id,
        flyt_fra=fra_person.navn,
        flyt_til=til_person.navn,
        flyt_fra_bruker_ident=fra_person.bruker_ident,
        flyt_til_bruker_ident=til_person.bruker_ident,
        flyt_mottatt_dato=mottatt_dato,
        flyt_sendt_dato=sendt_dato,
        flyt_status=status,
    )
    add_link(df, df.journalpost_link, jp_saved)

    return df


def add_link(src: NoarkEntity, field_name: Any, link_to_entity: NoarkEntity) -> None:
    """Add links when the object is empty"""
    if link_to_entity:
        if link_to_entity.id is None:
            msg = f"Cant link src type: {type(src)}, id: {src.id} to {type(link_to_entity)}"
            raise InvalidLink(src, field_name, link_to_entity, msg)

        src.links[field_name] = link_to_entity.id


def add_link_list(
    src: NoarkEntity, field_name: Any, link_to_entities: List[NoarkEntity]
) -> None:
    """Add links when the object is empty"""
    if link_to_entities:
        for link_entity in link_to_entities:
            if link_entity.id is None:
                msg = f"Cant link entities src type: {type(src)}, id: {src.id} to {type(link_to_entities)}"
                raise InvalidLink(src, field_name, link_to_entities, msg)

        link_entities = []
        for ent in link_to_entities:
            link_entities.append(ent.id)
        src.links[field_name] = link_entities


def validate_id(entity: NoarkEntity) -> None:
    if not entity:
        raise ValueError(
            f"Required valid entity, type: {entity.__name__}, but was None"
        )
    if not entity.id:
        raise ValueError(f"Required valid entity.id: {entity.id}, but was None")


def parse_links(kwargs: Any) -> Dict[Any, Any]:
    links: Dict[Any, Any] = {}
    if not kwargs:
        return links
    for field_name, field_val in kwargs.items():
        if "link" in field_name:
            links[field_name] = field_val
            kwargs.pop(field_name)
    return links

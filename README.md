# Documaster Client
Client for accessing [Documaster rest api](https://documaster.github.io/api-docs/): 


## Getting started

1. Git clone, install and run pytest  
`
 poetry install && 
 poetry run pytest
`

2. Copy config.example.yaml and update the Gravitee API Key
3. Run integration test
`
 poetry run pytest -m integration 
`
## Authentication
Documaster support self-sigend certificates with JWT.  
The client assumes that the authentication is handled in Gravitee. Normally this is done 
when the Gravitee plan is created. 
The client then will use the `Gravitee-API-KEY` from the subscription.  


## Documaster env
At the moment there are following environments:

| env  | url                                                   |
|------|-------------------------------------------------------|
| dev  | https://uhsak-2s-preview.dev.documaster.tech          |
| test | https://uhptest-hist.documaster.no                    |
| prod | https://uhptest-prod.documaster.no |

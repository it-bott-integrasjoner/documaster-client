import pytest

from documaster_client import query_util
from documaster_client.client import DocumasterClient
from documaster_client.model import Klassifikasjonssystem


@pytest.mark.integration
def test_get_by_id(iclient: DocumasterClient):
    kls = query_util.get_by_id(iclient, Klassifikasjonssystem, "847")
    assert kls
    assert kls.id == "847"


@pytest.mark.integration
def test_get_klasse_w_prefix(iclient):
    klasse = query_util.get_klasse_w_prefix(iclient, "847", ".1044")
    assert klasse
    assert ".1044" in klasse.klasse_ident


@pytest.mark.integration
def test_get_klasse_w_prefix_found_none(iclient):
    with pytest.raises(ValueError):
        query_util.get_klasse_w_prefix(iclient, "847", ".invalid")

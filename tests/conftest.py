import json
import os
from pathlib import Path
from typing import Union, Any, Dict, Iterable, SupportsIndex

import pytest

from documaster_client.client import DocumasterClient
from documaster_client.model import (
    DocumasterClientConfigLoader,
    Doc,
    DocumasterClientConfig,
)


def load_file(
    file: Union[Path, str], rb: str = "r"
) -> Union[Iterable[SupportsIndex], Any]:
    file = Path(file)
    if not file.exists():
        raise FileNotFoundError("Missing file: " + str(file))
    with open(file, rb) as f:
        return f.read()


def load_json_file(name: str) -> Dict[str, str]:
    here = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
    with open(os.path.join(here, "fixtures", name)) as f:
        data = json.load(f)
    return data


@pytest.fixture
def transaction_resp_create() -> Dict[str, str]:
    return load_json_file("resp/arkiv-transaction-resp.json")


@pytest.fixture
def documaster_client(example_config: DocumasterClientConfigLoader) -> DocumasterClient:
    example_config.documaster_client.code_list.cache = False
    example_client = DocumasterClient(example_config.documaster_client)
    return example_client


# Used in integration tests, default: :disabled
@pytest.fixture
def iclient() -> DocumasterClient:
    """Using uib sandbox as integration test env"""
    client_config = DocumasterClientConfigLoader.from_file(
        "config-sandbox-uib.yaml"
    ).documaster_client
    return DocumasterClient(client_config)


@pytest.fixture
def integr_config() -> DocumasterClientConfig:
    client_config = DocumasterClientConfigLoader.from_file(
        "./config.yaml"
    ).documaster_client
    return client_config


# Used in integration tests, default: :disabled
@pytest.fixture
def example_config() -> DocumasterClientConfigLoader:
    return DocumasterClientConfigLoader.from_file("./config.example.yaml")


def test_data():
    return {"akriv_skaper": 67123, "arkiv": 67122}


@pytest.fixture
def transaction_req_arkiv_dict() -> Dict[str, str]:
    return load_json_file("req/arkiv-save-transaction-req.json")


@pytest.fixture
def transaction_req_ekstern_id_dict() -> Any:
    return load_json_file("req/transaction-req-ekstern-id.json")


@pytest.fixture
def transaction_resp_arkiv_klasse_dict() -> Any:
    return load_json_file("resp/arkiv-klasse-transaction-resp.json")


@pytest.fixture
def transaction_req_arkiv_klasse_dict() -> Any:
    return load_json_file("req/arkiv-klasse-transaction-req.json")


@pytest.fixture
def transaction_req_dokument_flyt_dict() -> Any:
    return load_json_file("req/transaction-req-dokument-flyt.json")


@pytest.fixture
def arkiv_query_none_clause_dict() -> Any:
    return load_json_file("req/query-arkiv-none-clause.json")


@pytest.fixture
def saksmappe_model_dict() -> Any:
    return load_json_file("model/saksmappe.json")


def pytest_collection_modifyitems(config: Any, items: Dict[Any, Any]) -> Any:
    if config.option.keyword or config.option.markexpr:
        return
    skip_integration = pytest.mark.skip(
        reason='Not running with pytest -m "integration"'
    )
    for item in items:
        if "integration" in item.keywords:
            item.add_marker(skip_integration)


@pytest.fixture
def test_doc() -> Doc:
    content = load_file("./tests/fixtures/docs/simple.pdf", "rb")
    content = bytes(content)
    return Doc(name="simple.pdf", title="simple pdf file", content=content)

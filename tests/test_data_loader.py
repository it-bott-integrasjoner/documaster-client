from typing import Dict, Any

import pytest

from documaster_client.client import DocumasterClient
from documaster_client.model import (
    Arkivdel,
    Klassifikasjonssystem,
    Klasse,
)

test_data = {"akriv_skaper": 67123, "arkiv": 67122}


@pytest.mark.integration
def _test_arkiv_klasse_creation(
    iclient: DocumasterClient, transaction_req_arkiv_klasse_dict: Dict[Any, Any]
) -> None:
    arkivdel = Arkivdel(
        id="arkiv-del-temp-id-1", tittel="Arkivdel_1", beskrivelse="Arkivdel_1"
    )
    klassifikasjonssystem = Klassifikasjonssystem(
        id="klassifikasjonssystem-temp-id-1", tittel="klassifikasjonssystem 1"
    )
    klasse = Klasse(id="klasse-temp-id-1", klasse_ident="klasse1", tittel="klasse 1")

    arkivdel.links[arkivdel.arkiv_link] = str(67122)
    arkivdel.links[
        arkivdel.primaer_klassifikasjonssystem_link
    ] = klassifikasjonssystem.id
    klasse.links[klasse.klassifikasjonssystem_link] = klassifikasjonssystem.id
    entities = [arkivdel, klassifikasjonssystem, klasse]
    req = iclient.transaction_req(entities)
    assert transaction_req_arkiv_klasse_dict == req.dict()
    resp = iclient._post_transaction(req)
    assert resp

from documaster_client.model import (
    Sakspart,
    JournalpostType,
    Korrespondanseparttype,
)
from documaster_client.noark import (
    create_arkiv_skaper,
    create_saksmappe,
    create_jp,
    create_arkiv_del,
    create_arkiv,
    create_klassering,
    create_klassifikasjonssystem,
    create_korrespondansepart,
)


def test_create_arkiv_skaper() -> None:
    skaper = create_arkiv_skaper(aid="ident", ident="ident", navn="navn")
    assert skaper.arkivskaper_navn == "navn"
    assert skaper.arkivskaper_ident == "ident"


def test_create_saksmappe() -> None:
    sakspart = Sakspart(
        id="sak1",
        sakspart_navn="olav hansen",
        sakspart_ident="olavh@ntnu.no",
        sakspart_rolle="rolle1",
    )

    arkivskaper = create_arkiv_skaper(aid="ident", ident="ident", navn="navn")

    arkiv = create_arkiv(
        aid="tmp-arkiv", tittel="test arkivdel", arkivskaper=arkivskaper
    )

    klsystem = create_klassifikasjonssystem(
        aid="klassystem-1", tittel="test prim klassifikasjonssystem"
    )

    prim_klasse = create_klassering(
        aid="klasse-1", ident="klasse_test1", tittel="klasse_test1", klsystem=klsystem
    )

    arkivdel = create_arkiv_del(
        aid="tmp-arkivdel", tittel="test arkivdel", arkiv=arkiv, prim_klsystem=klsystem
    )

    sak = create_saksmappe(
        aid="sak1",
        tittel="en sak",
        admin_enhet="TestDepartment",
        arkivdel=arkivdel,
        sakspart=sakspart,
        prim_klasse=prim_klasse,
        mappe_ident="mapp1",
    )

    assert sak
    assert sak.id == "sak1"
    assert sak.tittel == "en sak"


def test_create_jp() -> None:
    skaper = create_arkiv_skaper(aid="ident", ident="ident", navn="navn")
    arkiv = create_arkiv(aid="tmp-arkiv", tittel="test arkivdel", arkivskaper=skaper)

    klsystem = create_klassifikasjonssystem(
        aid="klassystem-1", tittel="test prim klassifikasjonssystem"
    )

    prim_klasse = create_klassering(
        aid="klasse-1", ident="klasse_test1", tittel="klasse_test1", klsystem=klsystem
    )
    sek_klasse = create_klassering(
        aid="klasse-1", ident="klasse_test1", tittel="klasse_test1", klsystem=klsystem
    )

    arkivdel = create_arkiv_del(
        aid="tmp-arkivdel", tittel="test arkivdel", arkiv=arkiv, prim_klsystem=klsystem
    )

    korr_avsender = create_korrespondansepart(
        "korr1",
        "korrespondanse part navn",
        "TestDepartment",
        Korrespondanseparttype.avsender,
    )

    sakspart = Sakspart(
        id="saks-part-1",
        sakspart_navn="olav hansen",
        sakspart_ident="olavh@ntnu.no",
        sakspart_rolle="rolle1",
    )
    sak = create_saksmappe(
        aid="sak1",
        tittel="en sak",
        admin_enhet="TestDepartment",
        arkivdel=arkivdel,
        sakspart=sakspart,
        prim_klasse=prim_klasse,
        mappe_ident="mapp1",
    )

    jp = create_jp(
        aid="jp1",
        tittel="jp tittel",
        jp_type=JournalpostType.inngaaende_dokument,
        saksmappe=sak,
        korrespndanseparter=[korr_avsender],
    )

    assert jp
    assert jp.id == "jp1"
    assert jp.tittel == "jp tittel"
    assert jp.journalposttype == JournalpostType.inngaaende_dokument
    assert jp.links[jp.korrespondansepart_link] == ["korr1"]

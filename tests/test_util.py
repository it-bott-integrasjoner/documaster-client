from typing import Any

from documaster_client.util import chop_list


def test_chop_list_chopped(documaster_client: Any) -> None:
    list = []
    for i in range(0, 250):
        list.append(i)
    chopped_list = chop_list(list, 100)
    assert len(chopped_list) == 3
    assert len(chopped_list[0]) == 100
    assert len(chopped_list[1]) == 100
    assert len(chopped_list[2]) == 50


def test_chop_list_un_chopped(documaster_client: Any) -> None:
    list = []
    for i in range(0, 50):
        list.append(i)
    chopped_list = chop_list(list, 100)
    assert len(chopped_list) == 1
    assert len(chopped_list[0]) == 50


def test_chop_list_nth_max(documaster_client: Any) -> None:
    list = []
    for i in range(0, 100):
        list.append(i)
    chopped_list = chop_list(list, 100)
    assert len(chopped_list) == 1


def test_chop_list_nth_empty_rest(documaster_client: Any) -> None:
    list = []
    for i in range(0, 300):
        list.append(i)
    chopped_list = chop_list(list, 100)
    assert len(chopped_list) == 3

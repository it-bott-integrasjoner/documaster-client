import logging

import pytest

from documaster_client.client import DocumasterClient
from documaster_client.model import (
    TransactionRequest,
    LinkAction,
    SaveAction,
    Arkivskaper,
    Arkiv,
    UnsuccessfulRequestException,
    Doc,
    NoarkCode,
)

logger = logging.getLogger(__name__)


@pytest.mark.integration
def test_client_is_loaded(iclient: DocumasterClient) -> None:
    assert iclient is not None


@pytest.mark.integration
def test_transaction_request2(iclient: DocumasterClient) -> None:
    action_arkiv = SaveAction(
        type="Arkiv",
        id="arkiv-temp-id-1",
        fields={
            "tittel": "Test Fonds - Integration",
            "beskrivelse": "API Create Fonds - Integration",
        },
    )
    action_arkiv_skaper = SaveAction(
        type="Arkivskaper",
        id="arkivskaper-temp-id-1",
        fields={
            "arkivskaperIdent": "NTNU-integration",
            "arkivskaperNavn": "Integration Test",
        },
    )
    action_arkiv_skaper2 = SaveAction(
        type="Arkivskaper",
        id="arkivskaper-temp-id-2",
        fields={
            "arkivskaperIdent": "NTNU-integration-2",
            "arkivskaperNavn": "Integration Test-2",
        },
    )

    link = LinkAction(
        type="Arkiv",
        id="arkiv-temp-id-1",
        ref="refArkivskaper",
        linkToId="arkivskaper-temp-id-1",
    )

    action_arkiv2 = SaveAction(
        type="Arkiv",
        id="arkiv-temp-id-2",
        fields={
            "tittel": "Test Fonds - Integration-2",
            "beskrivelse": "API Create Fonds - Integration-2",
        },
    )

    link2 = LinkAction(
        type="Arkiv",
        id="arkiv-temp-id-2",
        ref="refArkivskaper",
        linkToId="arkivskaper-temp-id-2",
    )

    req = TransactionRequest(
        actions=[
            action_arkiv,
            action_arkiv2,
            action_arkiv_skaper,
            action_arkiv_skaper2,
            link,
            link2,
        ]
    )
    resp = iclient._post_transaction(req)
    assert resp is not None


@pytest.mark.integration
def test_gen_transaction_request(iclient: DocumasterClient) -> None:
    ak = Arkivskaper(
        id="arkivskaper-temp-id-1",
        arkivskaper_ident="uh-itest",
        arkivskaper_navn="Test Fonds - Integration-2",
    )
    arkiv = Arkiv(id="arkiv-temp-id-1", tittel="en-arkiv")
    arkiv.links[arkiv.arkivskaper_link] = ak.id

    req = iclient.transaction_req([ak, arkiv])
    resp = iclient._post_transaction(req)
    assert resp is not None


@pytest.mark.integration
def test_gen_transaction_request_existing_arkiv_skaper(
    iclient: DocumasterClient,
) -> None:
    arkiv = Arkiv(id="arkiv-temp-id-1", tittel="en-arkiv")
    arkiv.links[arkiv.arkivskaper_link] = "68558"

    req = iclient.transaction_req([arkiv])
    try:
        resp = iclient._post_transaction(req)
    except UnsuccessfulRequestException as ue:
        return
    raise AssertionError("")


@pytest.mark.integration
def test_upload_document_integr(iclient: DocumasterClient, test_doc: Doc) -> None:
    test_doc.name = "øver-på-fil.pdf"
    docs = iclient.upload_documents(docs=[test_doc])

    assert docs
    assert docs[0].id


@pytest.mark.integration
def test_document_type_code_list(iclient: DocumasterClient) -> None:
    code_list = iclient.get_code_list("dokumenttype")
    assert not None
    assert len(code_list.results)
    for code in code_list.results:
        assert code.field == "dokumenttype"
        assert code.type == "Dokument"
        assert code.values
        print("code", code.json())


@pytest.mark.integration
def test_save_code_list(iclient: DocumasterClient) -> None:
    code = "MEDSTUD"
    adm_enhet = NoarkCode(
        **{
            "name": "Adm-HR",
            "description": "brukt for å teste oppretting av admEnhet",
        }
    )
    try:
        created = iclient.save_code_list("administrativEnhet", adm_enhet)
        logging.info(f"Created administrativEnhet: {created}")

        resp = iclient.get_code_list("administrativEnhet", "Saksmappe")
        logger.info("created codelists %s" % resp)
        found = False
        for noark_code in resp.results[0].values:
            if noark_code.code == "akonym-org-enehet-test1":
                found = True

        assert found

    except Exception as err:
        msg = "Could not create adm enhet: %s, err: %s" % adm_enhet.name, str(err)
        logger.error(msg, exc_info=True)
        assert False

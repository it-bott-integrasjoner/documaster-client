import json
from typing import Dict, Any
from unittest.mock import Mock

from documaster_client.client import DocumasterClient
from documaster_client.model import (
    Arkivdel,
    Klasse,
    Klassifikasjonssystem,
    TransactionResp,
    Arkivskaper,
    Arkiv,
    SortOrder,
    ValueOperator,
    Oper,
    CompOper,
    Doc,
)
from documaster_client.noark import create_ekstern_id


def test_example_client(documaster_client: DocumasterClient) -> None:
    """Test that config is loaded"""
    assert documaster_client is not None


def test_create_transaction_req(documaster_client: DocumasterClient) -> None:
    ad = Arkivdel(id="tmp-arkiv-1", tittel="akrivdel1")
    ks = Klassifikasjonssystem(
        id="tmp-ks-1", tittel="klassessystem1", beskrivelse="klassesystem1"
    )
    kl = Klasse(id="tmp-k-1", klasse_ident="kl1", tittel="Klasse1")

    ad.links[ad.arkiv_link] = str(67122)
    ad.links[ad.primaer_klassifikasjonssystem_link] = ks.id
    kl.links[kl.klassifikasjonssystem_link] = ks.id
    entities = [ad, ks, kl]
    req = documaster_client.transaction_req(entities)

    assert req is not None


def test_create_transaction_req_ekstern_id(
    documaster_client: DocumasterClient, transaction_req_ekstern_id_dict: Dict[Any, Any]
) -> None:
    ekstern_el = create_ekstern_id("1223", "ekstern_id", "ekstern_system")
    req = documaster_client.transaction_req([ekstern_el])
    assert req
    assert transaction_req_ekstern_id_dict == req.dict()


def test_handle_transaction_resp(
    documaster_client: DocumasterClient, transaction_resp_create: TransactionResp
) -> None:
    resp = TransactionResp(**transaction_resp_create)

    saved_dict = documaster_client.create_saved_objects(resp)

    assert saved_dict
    arkivskaper: Arkivskaper = saved_dict["arkivskaper-temp-d-formidling"]
    # assert arkivskaper.__name__ == "documaster_client.model.Arkivskaper"
    a = {"Arkivskaper", Arkivskaper}
    assert type(arkivskaper).__name__ == "Arkivskaper"
    assert arkivskaper.id == "70825"
    assert arkivskaper.version == "3"
    assert arkivskaper.arkivskaper_ident == "UHS-d-formidling"
    assert arkivskaper.arkivskaper_navn == "UHS-d-formidling"


def test_create_query(documaster_client: DocumasterClient) -> None:
    qr = documaster_client.new_query(
        cls=Arkiv.__name__,
        fields={"id": "1234"},
        sort=SortOrder(field="id"),
        limit=10,
    )
    assert qr
    assert qr.query == "id = @paramid"
    assert qr.type == Arkiv.__name__
    assert qr.parameters == {"@paramid": "1234"}
    assert qr.limit == 10

    json = qr.json(by_alias=True)
    print("json: ", json)
    assert json
    assert "sortOrder" in json


def test_create_query_with_null_claus(
    documaster_client: DocumasterClient, arkiv_query_none_clause_dict: Dict[str, str]
) -> None:
    query_req = documaster_client.new_query(
        cls=Arkiv.__name__,
        fields={
            "tittel": ValueOperator(field_value=None, comp_oper=CompOper.LIKE),
            "beskrivelse": ValueOperator(
                field_value="en beskrivelse", oper=Oper.AND, comp_oper=CompOper.LIKE
            ),
        },
        sort=SortOrder(field="id"),
        limit=10,
    )
    assert query_req
    assert query_req.query == "tittel = @paramtittel && beskrivelse = @parambeskrivelse"
    assert query_req.type == Arkiv.__name__
    assert query_req.parameters["@paramtittel"] is None
    assert query_req.parameters["@parambeskrivelse"] == "en beskrivelse"
    assert query_req.limit == 10

    json = query_req.json(by_alias=True, exclude_none=True)
    print("json: ", json)
    assert json
    assert (
        query_req.dict(by_alias=True, exclude_none=True) == arkiv_query_none_clause_dict
    )


def test_upload_documents(documaster_client: DocumasterClient, test_doc: Doc) -> None:
    documaster_client._post_upload = Mock()
    documaster_client._post_upload.return_value = json.loads('{"id": "3212121" }')
    documaster_client.upload_documents(docs=[test_doc])

    docs = documaster_client.upload_documents(docs=[test_doc])
    assert docs
    assert docs[0].id

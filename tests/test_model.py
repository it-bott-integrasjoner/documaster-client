import json
from datetime import datetime
from typing import Dict

from documaster_client.client import DocumasterClient
from documaster_client.model import (
    SaveAction,
    LinkAction,
    TransactionRequest,
    TransactionResp,
    Klasse,
    Arkivdel,
    Klassifikasjonssystem,
    UnsuccessfulRequestException,
    Saksmappe,
    Saksstatus,
    Journalpost,
    Dokumentflyt,
    FlytStatus,
    JournalpostType,
)


def test_transaction_request(transaction_req_arkiv_dict: Dict[str, str]) -> None:
    action_arkiv = SaveAction(
        type="Arkiv",
        id="arkiv-temp-id-1",
        fields={
            "tittel": "Test Fonds - Integration",
            "beskrivelse": "API Create Fonds - Integration",
        },
    )
    action_arkiv_skaper = SaveAction(
        type="Arkivskaper",
        id="arkivskaper-temp-id-1",
        fields={
            "arkivskaperIdent": "NTNU-integration",
            "arkivskaperNavn": "Integration Test",
        },
    )

    link = LinkAction(
        type="Arkiv",
        id="arkiv-temp-id-1",
        ref="refArkivskaper",
        linkToId="arkivskaper-temp-id-1",
    )
    req = TransactionRequest(actions=[action_arkiv, action_arkiv_skaper, link])
    assert req.dict() == transaction_req_arkiv_dict


def test_create_transaction_resp(
    transaction_resp_arkiv_klasse_dict: Dict[str, str]
) -> None:
    resp = TransactionResp(**transaction_resp_arkiv_klasse_dict)
    assert resp
    assert resp.saved["arkiv-del-temp-id-1"]
    assert resp.saved["klassifikasjonssystem-temp-id-1"]
    assert resp.saved["klasse-temp-id-1"].type == Klasse.__name__
    assert resp.saved["arkiv-del-temp-id-1"].type == Arkivdel.__name__
    assert (
        resp.saved["klassifikasjonssystem-temp-id-1"].type
        == Klassifikasjonssystem.__name__
    )


def test_create_saksmappe(saksmappe_model_dict: Dict[str, str]) -> None:
    mappe = Saksmappe(**saksmappe_model_dict)

    assert mappe.id == "1131"
    assert mappe.saksstatus == Saksstatus.under_behandling
    assert mappe.saksstatus == Saksstatus.under_behandling


def test_create_jp() -> None:
    jp = Journalpost(
        id="tmp1", tittel="tittel", journalposttype=JournalpostType.inngaaende_dokument
    )
    assert jp


def test_create_dokument_flyt(
    documaster_client: DocumasterClient,
    transaction_req_dokument_flyt_dict: Dict[str, str],
) -> None:
    flyt_sendt_dato = datetime(year=2022, month=7, day=1, hour=8, minute=20)
    flyt_mottatt_dato = datetime(year=2022, month=8, day=1, hour=15, minute=9)

    jp_id = "jp-1"
    df = Dokumentflyt(
        id="dokument-flyt" + jp_id,
        flyt_fra="Hansen olav",
        flyt_til="Ola Brakke",
        flyt_fra_bruker_ident="fra_bruker_ident",
        flyt_til_bruker_ident="fra_bruker_ident",
        flyt_mottatt_dato=flyt_mottatt_dato,
        flyt_sendt_dato=flyt_sendt_dato,
        flyt_status=FlytStatus.godkjent,
    )

    saved = documaster_client.transaction_req(entities=[df], saved={})
    assert saved.json() == json.dumps(transaction_req_dokument_flyt_dict)


def test_UnsuccessfulRequestException() -> None:
    try:
        raise UnsuccessfulRequestException(
            "http://example.com", "Invalid field name for Arkiv"
        )
    except UnsuccessfulRequestException as e:
        assert e.url == "http://example.com"
        assert e.msg == "Invalid field name for Arkiv"

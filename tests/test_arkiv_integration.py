import datetime
from copy import deepcopy
from typing import Any

import pytest
import time

from documaster_client.client import DocumasterClient
from documaster_client.model import (
    Klassifikasjonssystem,
    SortOrder,
    NoarkCode,
    Sakspart,
    JournalpostType,
    Korrespondanseparttype,
    FlytStatus,
    Person,
    Avskrivning,
    AvskrivingMaate,
    DocumasterClientConfigLoader,
)
from documaster_client.noark import (
    create_klassering,
    create_arkiv_skaper,
    create_arkiv_del,
    create_arkiv,
    create_klassifikasjonssystem,
    create_jp,
    create_saksmappe,
    create_korrespondansepart,
    create_dokument_flyt,
    create_mappe,
    create_basis_reg,
)


@pytest.mark.integration
def test_query_klasse(iclient: DocumasterClient) -> None:
    query_req = iclient.new_query(
        Klassifikasjonssystem.__name__,
        fields={"tittel": "Person"},
        limit=1,
        sort=SortOrder(field="tittel"),
    )

    result = iclient.query(query_req)

    assert result
    kls: Klassifikasjonssystem = list(result.values())[0]
    assert kls.tittel == "Person"


@pytest.mark.integration
def test_dokumentmedium(iclient: DocumasterClient) -> None:
    adm = NoarkCode(**{"name": "Administrasjon1", "code": "PA", "description": "Admin"})
    adm = iclient.save_code_list(field="dokumentmedium", code_list=adm)

    assert adm


@pytest.mark.integration
def test_insert_document_type(iclient: DocumasterClient) -> None:
    #     adm = NoarkCode(
    #         **{
    #             "name": "Administrasjon" + str(i),
    #             "code": "adm" + str(i),
    #             "description": "Admin",
    #         }
    #     )
    #     adm = iclient.save_code_list(field="administrativEnhet", code_list=adm)
    # administrativEnhet
    resp = iclient.get_code_list("administrativEnhet")
    assert resp


@pytest.mark.integration
def test_adm_enhet(iclient: DocumasterClient) -> None:
    # for i in range(100):
    #     adm = NoarkCode(
    #         **{
    #             "name": "Administrasjon" + str(i),
    #             "code": "adm" + str(i),
    #             "description": "Admin",
    #         }
    #     )
    #     adm = iclient.save_code_list(field="administrativEnhet", code_list=adm)
    # administrativEnhet
    resp = iclient.get_code_list("administrativEnhet")
    assert resp


@pytest.mark.integration
def test_arkiv_save1(iclient: DocumasterClient, test_doc: Any) -> None:
    print("started...")
    loadtest_config = DocumasterClientConfigLoader.from_file("config.yaml")

    # NB override the client to sue sandbox env
    # sak_creator = AllCreator(dlt_config=loadtest_config.documaster_dataloader)
    iclient = DocumasterClient(config=loadtest_config.documaster_client)
    # adm_enhet = sak_creator.get_adm_enhet()
    # arkiver_map = sak_creator.get_arkiv(loadtest_config.documaster_dataloader.create_config.arkivskaper.ident)
    #
    saved = {}
    # save arkivskaper
    config_arkivskaper = loadtest_config.documaster_client.arkiv_skaper
    arkivskaper = create_arkiv_skaper(
        aid=config_arkivskaper.indent,
        ident=config_arkivskaper.indent,
        navn="FUP Arkiv skaper",
    )
    arkiv = create_arkiv(aid="fup_arkiv", tittel="FUP Arkiv", arkivskaper=arkivskaper)
    saved = iclient.bulk_transaction_call([arkivskaper, arkiv], {})
    arkiv = saved["fup_arkiv"]

    # save klsystem
    klsystem_prim = create_klassifikasjonssystem(
        aid="klassystem", tittel="fup prim klassifikasjonssystem"
    )

    klsystem_sek = create_klassifikasjonssystem(
        aid="klassystem-2", tittel="fup sek klassifikasjonssystem"
    )

    # noinspection PyTypeChecker
    arkivdel = create_arkiv_del(
        aid="tmp-a_d",
        tittel="A.d",
        arkiv=arkiv,
        prim_klsystem=klsystem_prim,
        sek_klsystem=klsystem_sek,
    )

    prim_klasse = create_klassering(
        aid="klasse",
        ident="A.d.02",
        tittel="A.d.02 Ansette",
        klsystem=klsystem_prim,
    )

    sek_klasse = create_klassering(
        aid="klasse-2",
        ident="klasse_test2",
        tittel="klasse_test2",
        klsystem=klsystem_sek,
    )
    saved = iclient.bulk_transaction_call(
        [klsystem_prim, arkivdel, prim_klasse, klsystem_sek, sek_klasse], saved
    )

    prim_klasse = saved[prim_klasse.id]
    arkivdel = saved[arkivdel.id]
    sek_klasse = saved[sek_klasse.id]
    klsystem_sek = saved[klsystem_sek.id]

    print(arkivskaper)
    print(arkiv)
    print(arkivdel)
    print(prim_klasse)
    print(klsystem_sek)

    sakpart = Sakspart(
        id="sakpart",
        sakspart_navn="Test bruker",
        sakspart_rolle="internal",
    )
    # noinspection PyTypeChecker
    mappe = create_mappe("tmp-mappe", "nytt mappe", arkivdel, prim_klasse, sek_klasse)

    basis_reg = create_basis_reg("tmp-basis-reg", "nytt basis registrering", mappe)
    # noinspection PyTypeChecker
    saksmappe = create_saksmappe(
        aid="sak1",
        prefiks="SAP",
        tittel="sak test1",
        admin_enhet="TestDepartment",
        arkivdel=arkivdel,
        sakspart=sakpart,
        prim_klasse=prim_klasse,
        sek_klasse=sek_klasse,
        mappe_ident="saksmappe1",
    )
    korr_part = create_korrespondansepart(
        "korrespondanse part1",
        "Mottaker Nilsen",
        "TestDepartment",
        Korrespondanseparttype.mottaker,
    )
    # ekstern_id_el = create_ekstern_id(
    #     aid="ekstern",
    #     eks_id="last_saved_jp",
    #     eks_system="integr_system_pft",
    # )
    jp = create_jp(
        aid="jp",
        tittel="Jp uten jp år",
        saksmappe=saksmappe,
        jp_type=JournalpostType.organinternt_dokument_uten_oppfoelging,
        korrespndanseparter=[korr_part],
        # ekstern_id=ekstern_id_el,
    )

    result = iclient.bulk_transaction_call(
        [basis_reg, mappe, saksmappe, sakpart, korr_part, jp], saved
    )

    saved_jp = result[jp.id]
    assert jp.id

    dflyt = False
    signOff = False
    entities = []

    if dflyt:
        # noinspection PyTypeChecker
        dok_flyt = create_dokument_flyt(
            jp_saved=saved_jp,
            fra_person=Person(navn="Olav Hansen", bruker_ident="olav_hansen"),
            til_person=Person(navn="Bjarne henriksen", bruker_ident="bjarne_hendksen"),
            mottatt_dato=datetime.datetime.now(),
            sendt_dato=datetime.datetime.now(),
            status=FlytStatus.godkjent,
        )

        entities.append(dok_flyt)

    if signOff:
        avsk = Avskrivning(
            id="av-skriv-jp-id" + saved_jp.id,
            avskrivningsmaate=AvskrivingMaate.tatt_til_orientering,
        )
        avsk.links[avsk.journalpost_link] = saved_jp.id
        entities.append(avsk)

    upload_doc = False

    upload_count = 100
    start_time = time.time()
    if upload_doc:
        entities = []
        docs = []
        # 100 doc for 10 ganger
        for k in range(1):
            # doc hundre dokument 1 gang

            for idx in range(0, upload_count):
                a_doc = deepcopy(test_doc)
                docs.append(a_doc)

            iclient.upload_documents(docs=docs)
        # doc_entities = create_dokument_version(
        #     jp=saved_jp, docs=docs, dokument_type="n_notat"
        # )
        #
        # entities.extend(doc_entities)
        #
        # iclient.bulk_transaction_call(entities=entities, saved=saved)
        print("--- %s seconds ---" % (time.time() - start_time))

    assert saved

    if upload_doc:
        iclient.upload_documents(docs=[test_doc])

        dw_file = iclient.download_file(test_doc.id)
        assert dw_file
